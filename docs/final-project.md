## PDF A4 explicatif avec QR code disponible    [ici](images/présentation_A4.pdf)

# OBJET FINAL #
![](images/photos_produit_fini2.jpg)
L'en-bois-table est un mobilier simple et rapide à réaliser qui peut s'adapter facilement en fonction de vos besoins. Idéal pour les petits appartements, il est aisé d'alterner entre un **plateau de lit**, une **table de nuit** ou encore une **table d'appoint** à l'aide d'attaches spécialement conçues dans ce but. Plus besoin de s'encombrer d'une floppée d'équipements !

### EXPLICATIONS ###
![](images/gif_.gif)

Pour réaliser ce mobilier, j'ai d'abord imaginé un module de base qui pourrait être utilisé de différentes manières en le duplicant et en faisant une rotation sur lui-même. Pour ce faire, il a fallu que les dimensions du module de base soient calculées de sorte à ce que celles-ci puissent garantir une utilité confortable et pratique de l'objet. Ensuite, il m'a fallu réfléchir à un système de fixation qui permet de facilement transformer l'objet pour tout utilisateur et avec peu de moyen. Enfin, afin de rester dans cette logique de réaliser un meuble abordable et avec peu de moyen, ce mobilier à été conçu avec un panneau en MDF 12 mm 244x122mm dont le prix s'élève à 21,39 €.

### LES ATTACHES ###
Etant donnée le nombre  d'attaches développées, ci-dessous tu peux trouver un petit guide spécifiant l'emplacement de chaque attache pour ne pas te tromper. Voici également un **fichier SVG** pour pouvoir directement les découper à l'aide nottament de la Shaper origin pour ma part. [ici](images/SVG_Attaches.svg)

Si tu as besoin d'informations complémenntaire concernant l'utilisation de la Shaper origin, je t'invite à consulter mon module 5 que tu peux retrouver [ici](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/alizee.cosyns/modules/module05/).

![](images/PINCES2.jpg)

**Remarque :** Les attaches ont été conçues pour s'adapter à des plaques d'une épaisseur de 12mm. S'il te prenait l'envie de réaliser ce mobilier dans un matériau avec une autre épaisseur, il te faudrait modifier la largeur des interstices qui est actuelement de 12mm par l'épaisseur de ton matériau choisi.


# RECHERCHES #
Je t'invite ici à suivre mon parcours pour arriver au résultat final que tu as pu admirer ci-dessus.

### 1. ENONCE ###
Le travail final porte sur la **réinterprétation d'un objet** design choisi au musée belge ADAM qui se trouve au Heysel. Pour débuter cet exercice, il m'a fallu choisir un mot parmis une liste de cinq. Les critères étaient simples : choisir celui qui m'inspire le plus et en tirer "une ligne de conduite" qui m'aidera à achever ce nouveau projet. Afin de m'aider dans ce choix, nous avons eu à disposition une liste de définitions proposées par différents dictionnaires. Celle-ci est consultable [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/alizee.cosyns/-/blob/master/docs/images/design_mots_definition.pdf).

### 2. MOT CHOISI - INSPIRATION

### 2.1 Mon rapport au mot
Le mot que j’ai choisi est **inspiration**. Ce mot est celui qui me parle le plus parmi les cinq proposés (hommage-extension-référence-influence-inspiration) même si j'hésitais avec le mot influence qui, à mon sens, est proche en terme de signification.  

**Dans la vie de tous les jours**, je pense que tout le monde s’inspire de ce qui l’entoure et **reproduit consciemment ou inconsciemment des variantes de ce qui nous a marqué**. Notre quotidien peut-être une source d’inspiration en prêtant attention à notre entourage, notre mode de vie, la différence des cultures, l’environnement qui nous entoure, nos expériences passées, les personnes qui nous font rêver, une musique, une citation… Nous nous construisons autour de toutes ces choses qui peuvent nous permettre, selon moi, d’atteindre un sentiment de bien-être, de satisfaction ou nous permettent de se rapproche de notre philosophie de vie.

**Lors de la réalisation d’un projet**, nous allons tirer de notre mémoire ou d’un autre objet, des souvenirs, des sensations, des méthodes, des principes, des techniques… et les **transposer** dans une nouvelle création. Il ne s’agit pas de recopier bêtement ce que l’on a vu mais plutôt de faire penser à l’œuvre d’origine sans forcément la reconnaître au premier regard.

### 2.2 Deux sens majeurs au mot inspiration selon moi ###
Suite à la lecture des différentes définitions du mot inspiration, voici les deux sens majeurs que j’en retire :

1. 	L’inspiration est l’acte par lequel notre corps absorbe l’oxygène de l’air et rejette le dioxyde de carbone.
2.	Le souffle d’inspiration, le souffle créateur ‘’suggérer’’ par quelqu’un ou quelque chose.

En lisant ces deux ‘’nouvelles’’ définitions, à mon sens, elles se rejoignent sur le principe d’emmagasiner une information/une donnée/ un élément dont a besoin et le reste est mis de côté.

### 2.3 Les mots que m'inspirent le terme inspiration
observation – sentiment – sensation – ambiance – soudain – souvenir – mémoire – similitude – guide

* Observation : L’inspiration vient d’éléments qui ont été observés au préalable.
* Sentiment - sensation - ambiance : L’objet de l’inspiration évoque sans doute lui-même l’ambiance d’un lieu et vous fait ressentir une sensation/un sentiment lié à votre expérience personnel.
* Soudain - souvenir - mémoire : L’inspiration peu vous venir à tout coin de rue. Il suffit d’un élément qui vous embarque dans un souvenir lointain.
* Similitude - guide : L’objet de votre inspiration est un guide tout au long de la conception de votre projet. Des similitudes pourront s’en dégager.  

### 2.4 Illustrer mon propos à l'aide d'exemples concrets
Afin de mieux visualiser ce que je viens d’exposer, je t'invite à observer ces différents exemples :
1. **Dries Van Noten inspiré par le designer Panton**
* Au point de vue des couleurs : vert, bleu, orange, mauve
* Au point de vue des mots psychédéliques

![](images/Dries_Van_Noten.JPG)

2. **Gaudi inpiré par la forêt pour la nef centrale de Sagrada Familia**
* Au point de vue du ressenti du lieu

![](images/Gaudi-Sagrada_Familia.JPG)

### 3. RAPPEL DE MON OBJET CHOISI AU MUSEE ###
L'objet dont je vais m'inspirer pour réaliser mon objet final est la **Chaise Universale de Joe Colombo** réalisée en 1967. Un premier descriptif de cette objet est disponible sur ma page d'acceuil consutable [ici](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/alizee.cosyns/). Mais afin de savoir dans quelle direction partir et déterminer les lignes fortes de mon projet, je décide de m'informer d'avantage  sur la philosophie du designer Joe Colombo et la société Kartell.
![](images/rappelchaise.jpg)
![](images/chaise_universale2.jpg)


### 3.1 Entreprise Kartell ###
La Chaise Universale a été réalisée sur demande de l’entreprise **Kartell**. Il s’agit d’une société italienne **fondée en 1949** par Giulio Castelli. En 1988, elle sera rachetée par Claudio Luti, gendre de Giulio et Anna Castelli qui feront appel à de nombreux designers et architectes dont les produits deviendront rapidement des symboles de la marque.
![](images/kartell.jpeg)

L’entreprise Kartell est **spécialisée** dans la **fabrication de produits en plastiques** de luxe.  Au cours des années, elle a acquis une maîtrise dans différentes techniques pour fabriquer des produits de plus en plus complexes. L’emploi, la manipulation et le développement de **matières novatrices** sera un élément clef dans le parcours et l’histoire de Kartell afin d’obtenir un produit final inédit. Le fruit de ces recherches donnera naissance à la chaise La Marie, une chaise complètement transparente au **design moderne et minimaliste** suivie de la chaise Louis Ghost, l’un des modèles best-sellers de Kartell. Dès lors, l’entreprise développera et approfondira le **thème de la transparence** tout en poursuivant ses recherches dans l’**étude des surfaces et des formes**.

### 3.2 Le designer Joe Colombo ###
Le designer Joe Colombo est né à Milan an 1930. Il y a étudié la peinture puis l’architecture. Sa carrière de designer commence en 1962 lorsqu’il ouvre son propre studio de design.
Son style se caractérise par une approche innovante, futuriste, voire utopique.

![](images/Joe_Colombo.jpeg)

### 3.4 Conclusion ###
Suite à mes recherches, j’ai extrait plusieurs mots qui définissent les différents intervenants et la Chaise Universale. En recroisant ces trois brainstormings, il en est ressorti 3 mots : **Minimaliste – Fonctionnel – Modulable**. Ces 3 mots seront les concepts qui définiront mon projet.  
![](images/mots_porteurs2.jpg)
### 4. DEFINIR L’OBJET – TABLE ####
Rappel des concepts choisis : **Minimaliste – Fonctionnel – Modulable**
### 4.1 Quel mobilier choisir ###

Etant sensible au phénomène de consommer moins et mieux afin de réduire nos déchets, un objet modulable serait une manière de répondre à cette volonté. De plus, se déplacer, déménager étant devenu quelque chose de tout à fait normal, un mobilier capable de se transformer en fonction de l’utilisation dont on en fait, permettrait de se déplacer plus facilement et plus légèrement.

Ensuite, toujours dans l’esprit de consommer plus responsable et utile, j’ai réfléchi à quel était le mobilier que l’on achetait en premier et qui pouvait servir à différents emploies. La table est apparue pour moi comme un élément de base trouvant place dans chacun de nos habitats. On mange autour d’une table, on lit à coté d’une table, on dépose ses affaires sur une table, on joue sur une table… Il existe mainte emploie de cet objet. De plus, afin de rester sur le **principe de la Chaise Universale qui est, un objet est égale à trois**, l’objet table serait capable de se mettre dans trois position afin de convenir à différents besoins.
![](images/typedetable.jpg)


 ### 4.2 Comment être modulaire
 Objet modulable, oui mais par quel moyen ? Joe Colombo a rendu la Chaise Universal modulable à l’aide d'un système d’assemblage. En effet, il est possible de faire varier la hauteur de la chaise. Cela permet de faire varier l’emploie de celle-ci. Afin de déterminer quel moyen je vais utiliser pour ma table je commence par chercher quelques exemples.
![](images/etremodulable.jpg)

 ### 4.3 Parti pris - Module de base de l'objet

 Suite à ces recherches, j’ai décider de me concentrer sur la conception de l’objet table. Celui-ci deviendrait modulable en le faisant **tourner sur ses faces**. Le moyen de la rotation me parait une solution économique étant donné que je n’ai besoin que d’un unique élément contrairement à la Chaise Universale. Il s'agirait d'une **table d'appoint,d'une table de nuit et d'une table basse** qui mesurait 45x55x65 cm.
 ![](images/test1.jpg)



 ### 5. CHOIX DE LA MACHINE – DECOUPEUSE LASER

 Afin d’orienter mes recherches sur la conception de l’objet, j’ai dû prendre une décision sur le choix de la machine à employer. Mon objet final étant une table, il me fallait un moyen capable de produire/découper des éléments assez grand pour correspondre aux dimensions des différents types de tables choisies. De plus, n’étant pas fixée précisément sur le type de matériaux à employer, la découpeuse laser me permet de découper différents matériaux tel que le bois contreplaqué, l’acrylique, le carton ou encore le textile.

 ### 6. CHOIX DU MATERIAU - CARTON

 Une fois la machine déterminée, il faut décider quel matériau employer. Suites à mes recherches, le carton semblait pour moi une bonne solution. En effet, le carton permet de se meubler à moindre coût. Il est également écologique du fait qu’il est **100% réutilisable**. Le carton à l’avantage également de se **plier, s’emboiter, sans colle, ni clou**. De plus, il est facile à stocker et monter. Il est très **résistant** et reste malgré tout **léger** comparé au bois.

### 6.1 Les différents types de carton

* ##### Le carton alvéolaire
  * Léger
  * Très grande résistance à la compression (peut atteindre 40 tonnes au m2)
  * 100% recyclé
  * Colle utilisée essentiellement à base d’eau
  * Epaisseurs existantes : 10/15/25/50mm
  * C’est le carton le plus robuste. Il peut supporter jusqu’à 15 tonnes au m2 grâce à sa structure en nid d’abeilles. Il est composé de fibres végétales, sa durée de vie va de 10 à 20 ans.
* ##### Le carton en maille tressée
  * 70kg/m2
  * Épaisseurs existantes : 10/16mm
  * Couleur : blanc ou kraft
  * Euroclass C (M2) concernant la norme anti-feu (= difficilement combustible)
* ##### Le carton ondulé
  * Très résistant (du à sa fabrication) et utilisé pour généralement des meubles et des objets de décoration
  * Fabriqué à partir de plusieurs feuilles de papier cannelé collées les unes aux autres

### 7. TYPE D’ASSEMBLAGE CHOISI
![](images/typedassemblage.jpg)
Comme on peut le voir ci-dessus, il existe différentes façons d’assembler le carton pour obtenir un meuble. La technique que je préconise, est celle de l’emboitement car selon moi, elle nécessite moins de carton et surtout moins de colle même s’il existe des colles à base d'eau, je préfère éviter.

### 8. TEST 3D POUR VISUALISER MON ASSEMBLAGE + REMARQUES
![](images/testeassemblage.png)

Suite aux remarques lors du pré-jury, il m'a été vivement conseillé de **mieux étudier le dimensionnement** de chaque élément. En effet, une table de nuit n'a pas les mêmes proportions qu'une table de salon. De plus, le matériau carton est à proscrire. Il n'est pas adapaté à l'usage quotidien que l'on fait  d'une table. Je décide alors de me tourner vers le **bois**.

### 9. ETUDE DU DIMENSSIONNEMENT SUR BASE DES MEUBLES IKEA
* ##### Table d'appoint  
Conçue pour accueillir des objets que l'on veut garder à portée de main, comme un verre, un livre, une lampe de chevet… Si celle que l'on détient est particulièrement robuste, elle peut parfois servir de support d'ordinateur portable ou de plateau-repas devant la télévision.

![](images/tabledappoint.jpg)

  * ##### Les favorites

  ![](images/tabledappointfavorites.jpg)

* ##### Table pour enfant
La table d'enfant est idéale pour colorier, jouer ou manger.

![](images/tableenfant.jpg)

* ##### Table de nuit
Une table de nuit ou table de chevet est un petit meuble placé à côté d'un lit, à hauteur de la tête, et destiné à accueillir des choses utiles pour une nuit de sommeil, juste avant le coucher, ou au réveil : réveil, lampe, lunettes, téléphone, livre, verre d'eau, médicaments, etc. La hauteur d'une table de chevet n'excède généralement pas celle que l'on peut atteindre en étant allongé sur un lit, et elle peut être équipée de rangements tels que des niches, des tiroirs, ou des espaces fermés par des portes. La table de nuit est parfois intégrée au lit, notamment dans le cas des lits-ponts.

![](images/tabledenuit.jpg)

* ##### Table basse
Une table basse ou table de salon est un type de table suffisamment basse pour qu'on puisse y accéder tout en restant assis (d'où son nom), et qui est destinée à être placée dans une salle de séjour, à portée des personnes occupant le canapé et les fauteuils, afin qu'elles puissent y déposer ce dont elles ont l'usage lorsqu'elles sont ainsi installées. On y pose essentiellement les boissons qui vont être consommées, des magazines ou des livres, les télécommandes du matériel audio-vidéo, etc.

![](images/table_basse.jpg)

  * ##### Les favorites

  ![](images/tablebassefavorites.jpg)


### 10. DEFINIR LES 3 OBJETS
![](images/essaies.jpg)
Après quelques recherches...

### 10.1 Deux idées se démarquent
![](images/deuxidees.jpg)
### 10.2 Recherches de techniques d'assemblages
Il me fallait trouver un système d'assemblage facile à monter/démonter.
![](images/assemblages.jpg)
### 11. TEST EN MULTIPLEX
Afin d'augmenter la résistance de mon assemblage, je tente de produire un module en multiplex et avec des antailles car celui-ci resiste mieux en traction que le MDF utilisé.
![](images/MULTIPLEX.jpg)
Ayant fait une erreur au niveau des dimensions des espaces pour les attaches, il est difficile d'émettre un jugement sur le résultat malgré que j'ai essayé de les modifier en rajoutant du carton.

# AS TO BE CONTINUED...
