# _Salut à toi, visiteur de mon profil !_

### Ci-dessous quelques informations à mon sujet/sur mon travail qui t'intéresseront peut-être...



## About me

![](images/avatar-photo.jpg)    

Je m'appelle **Alizée** et je suis **étudiante master en architecture**. Après avoir séjourné un an à Vancouver, j'ai décidé de revenir en Belgique et de commencer mes études supérieures. J'ai longuement hésité entre architecture et graphisme mais j'en suis venue à la conclusion que la 3D, le graphisme, le multimédia... pouvaient également être des outils de travail pour les architectes. De ce fait, j'ai choisi de suivre l'**option Architecture & Design** à l'Université Libre de **Bruxelles** pour nourrir cet intérêt.

**Ici** tu pourras suivre l'évolution de mon travail pour cette option !

## My hobbies

Comme dis précédemment, je suis une personne qui aime découvrir et s'essayer à de nouvelles à habitudes/sports/réalisations...

Dans les **choses ''importantes'' à savoir sur moi** est que j'adore **chanter** même si je chante faux. La **danse** est pour moi l'une de mes plus grande passion même si aujourd'hui je ne pratique plus de façon régulière. J'aime également beaucoup les **sports d'eau**. Depuis que je suis petite et chaque été, ma famille et moi faisions de la planche à voile. J'ai grandi parmis les compétitions de voile. En effet, mon grand frère faisait des régates d'optimist et de splash. De ce fait, j'accompagnais souvent les weekends à la mer pour les entrainements. Je n'ai jamais voulu me lancer moi-même dans la compétition mais cet environnement m'a donné gout à essayer/faire des stage de kitesurf, laser pico, planches à voile... Nous avions même un mobilhome lorsque que nous devions nous rendre dans un pays pour les championnats internationals. C'est peut-être pour cela qu'aujourd'hui, je **rêverais de retaper un van et de partir en voyage avec**. Mon occupation actuelle lorsque j'ai du temps libre chez moi est la **couture**. J'ai recemment acheté une machine à coudre avec laquelle j'ajuste des vêtements achetés en seconde main. J'aime beaucoup le concept de **faire du neuf avec de l'ancien**. Dernièrement, j'ai transformé une robe en un haut dos nu pour l'été. En matière de style, j'ai un gout assez prononcé pour les vetêments, le mobilier et les objets de décorations **vintages**. Je trouve souvent les objets en brocantes d'une grande finesse et qui ont quelque chose à raconter...



## My background

J'ai obtenu mon diplôme de bachelier d'architecture à l'UCL Loci Saint-Gilles en 2020. A la suite de cela, j'ai changé d'école pour poursuivre ma formation car j'avais besoin d'un changement d'air et d'un plus large choix d'options. En effet, ne sachant pas encore exactement vers quoi je voudrais me diriger plus tard, il me semblait intêressant de s'essayer à tous mes centres d'intérets dont des cours plus accés sur différents outils de représentation. Je voudrais rajouter à cela que je suis une personne qui aime découvrir de nouvelles personnes/choses. Toutes expériences,selon moi est bonne à prendre. J'aime le changement et la surprise de ce qui m'attends encore !



## Projects

 * #### Réinterpréter un objet design du musée ADAM au Heysel

 ##### **La chaise Universale - Joe Colombo - 1967**

**Description :**

Il s'agit de la première chaise en ABS qui a été dessinée pr Joe C. Colombo en 1965. Elle fût moulée par injection, produite d'un seul tenant et empilable. A l'origine imaginée en aluminium, cette chaise sera finalement produite en plastique ABS par la maison d'édition Kartell afin d'en réduire les coûts. Elle se décline en une diversité de couleurs, de formats, de modèle, ect. Sa polyvalence et sa manoeuvrabilité couronnent la logique industrielle de la production mobilière en plastique qui marque les Swinging Sixties.

**Pourquoi ai-je choisi cet objet? :**

Au premier abord, cet objet qui est une chaise parait banal sans intérêt particulier. Elle  est de couleur sobre (blanche ou noir) et de forme que l’on s’imagine une chaise (une assise avec quatre pieds). Pourtant, elle occupe une place stratégique dans le musée et un podium entier lui est attribué.   

Ce qui m’interpelle le plus sont les croquis réalisés sur le mur du fond. Je  décide alors de m’en approcher et de lire la description pour en apprendre plus sur l’objet. Je comprends alors qu’il s’agit d’une chaise qui peut se transformer en tabouret de bar, en chaise pour enfant, en table basse... grâce à ses pieds amovibles. L’adaptabilité de cet objet est pour moi une caractéristique très intéressante. En effet, je trouve cela géniale de pouvoir en premier lieu la démonter et la transporter avec autant de facilité et en deuxième lieu transformer l’objet au gré des besoins de l’acquéreur. Cette objet banal n’est au final pas si banal que ça et s’intègre, selon moi, parfaitement dans cet air du temps où nous essayons de consommer nos objets le plus longtemps possible.

![This is another caption](images/sample-photo.jpg)


![This is the image caption](https://thegoodolddayz.files.wordpress.com/2015/12/chaises-universelles-joe-colombo-kartell-1967.jpg)
