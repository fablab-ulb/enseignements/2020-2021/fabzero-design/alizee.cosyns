# 5. Utilisation de la Shaper
![](../images/Shaper.jpg)
## 5.1 Introduction
Aujourd'hui j'ai suivi une formation sur la **Shaper Origin**. Il s'agit d'une petite CNC portable permettant de fraiser avec précision jusqu'à **43 mm** d'épaisseur. N'étant pas inscrite à la Formation de base, c'est **Maud** qui m'a expliqué comment l'utiliser. Je partagerai avec vous ici les informations essentielles qui m'ont été transmises. Pour plus d'information, je vous redirige sur le site officiel de la Shaper et tout particulièrement vers les tutoriels que vous trouverez [ici](https://www.shapertools.com/fr-fr/tutorials).


## 5.2 Avant utilisation
1. Il faut **fixer la planche** à l’aide de tape ou de visses pour éviter que le planche ne bouge lors de la découpe. **Cependant**, la machine ne doit jamais passer sur des vis. Cela risquerait de l’endommager. Il faut dans ce cas dévisser une vis s’il vous est impossible de faire autrement.

![](../images/clou.jpg)

2.  **Placer du shaper tape** environ tous les 7 cm afin que la Shaper détecte les découpes à faire/la zone de travail.

![](../images/placershapertape.jpg)

3.  **Paramétrer les différents réglages**
A.	Scan puis nouveau Scan pour détecter le tape

![](../images/scan.jpg)

B.	Dessiner pour importer son dessin **SVG**


C. Fraiser

![](../images/dessiner.jpg)

Dans fraiser vous devez d'abords indiquer jusqu'à quelle épaisseur vous voulez que la machine coupe. Ensuite vous pouvez choisir précisément où la machine va couper (à l'extérieur de la ligne de dessin, sur la ligne ou à l'intérieur ou pocher/creuser dans une partie de la matière). Enfin, vous devez indiquer le diamètre de la mèche que vous utilisé.

**Remarque :** Pour avoir la **découpe la plus net** possible, il faut parfois effectuer plusieurs petites découpes plutôt que vouloir couper à travers tout le matériau en un seul coup. **Pour connaitre le nombre de découpe à effectuer**, il faut diviser l’épaisseur du matériau par le diamètre de la mèche utilisée. Par exemple, si j’ai une planche d’épaisseur de 12 mm et que j’utilise une mèche de 3 mm, dans l’idéal, j’effectuerais 4 découpes.
4.  Placer sa clef USB et importer son dessin en allant dans DESSIN.

![](../images/importer.jpg)

**Remarque :** Il faut que mon dessin soit sous un **format SVG**.

5.  Mon dessin apparait sur l’écran. Je peux le déplacer en bougeant la machine pour choisir où la découpe va commencer sur ma planche. Une fois le dessin bien placé, j’appuie sur le bouton vert (sur la manette) pour dire placer.

![](../images/decoupe.jpg)

6.  **Appuyer sur la touche Z touch** dans fraiser à chaque fois que je modifie les paramètres ou que je fais une nouvelle découpe afin de s’assurer que les réglages soient bons et que la mèche touche bien le matériau.

## 5.3 Prêt à découper
1.  Allumer l’aspirateur

![](../images/aspirateur.jpg)

2.  Appuyer sur le bouton ON de la machine

![](../images/on.jpg)

3.  Appuyer sur le bouton vert de la manette pour lancer la découpe
**Remarques :**
* Les petits traits-tillés qui représente le trait de coupe bouge dans un sens. Ce sens indique vers où il faut déplacer la machine pour effectuer ma découpe.
* De préférence, commencer par découper les petites pièces et ensuite les grosses pièces afin d’éviter que votre planche ne bouge.

## 5.4 A propos des mèches
Au fablab, nous disposons de 3 mèches.

![](../images/mèches.jpg)

-	3 mm
-	6 mm
-	Pour faire les arrondis gravures
Lorsque je veux changer de mèches, je dois :
1. Débrancher la fraise de la machine pour éviter qu’elle ne se mette en route lorsque je change de mèche.
2. Dévisser la visse sur la partie qui fraise à l’aide d’un petit outil.
3. Une fois la partie contenant la fraise détachée du reste de la machine, je le retourne, la mèche se trouve vers le haut, vers moi. J’appuie ensuite sur le bouton et désère le boulon à l’aide d’une clef.
4. Je peux changer la mèche et ensuite tout refixer en veillant à ce que la mèche ne soit pas complètement enfoncée.

![](../images/changermèche.jpg)

## 5.4 Résulat
Ici on peut voir l'une des attaches que j'ai réalisée pour mon projet final consulstable [ici](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/alizee.cosyns/final-project/). La Shaper permet de couper avec précision des petites pièces tels que celle-ci.

![](../images/shaperresultat.jpg)
