# 1. Etat de l'art et documentation

L'option Architecture & Design débute pour moi par une formation de 5 semaimes sur différents programmes, machines... Ces formations ont pour but de nous aider afin de mener à bien le projet de ce quadrimestre qui est de réinterpréter un objet design des années 60-70 choisi au musée ADAM. Lors de ce **permier cours** sur place j'ai découvert **la platforme GitLab et le programme Fusion 360**. GitLab nous permet de documenter et référencer toute l'évolution de notre parcours lors de ce travail et Fusion 360 sera utilisé pour modéliser notre objet choisi avant de l'envoyer en impression 3D.


## **1.1 Définir mon objet du musée** ##

L'objet que j'avais choisi lors de la visite au musée ADAM était le buste Vénus de l'artiste Arman réalisé en 1971. le jeu de lumière dû à la matière et les lunettes qui composaient le buste en faisait un objet particulier. De plus, j'aimais beaucoup le fait que le spectateur disingue l'amas de lunettes que lorsqu'il se rapproche mettant ainsi en avant un ''déchet'' de surconsommation.  Malgré cela, j'ai décidé de changer d'objet car lors de mes recherches sur sa conception, je me suis demandé s'il faisait réellement parti des objets à considérer pour notre travail car aucune photographie, information n'était donnée à son sujet dans le livre que les étudiants avaient reçu à la fin de la visite du musée. De plus, même sur Google, il était fort difficile de trouver la réplique de ce buste car il existe une multitude d'oeuvre similaire mais remplie avec d'autres objets que des lunettes. Suite à cela, j'ai décidé de prendre la Chaise Universale de Joe Colombo, objet pour lequel j'avais longuement hésité à présenter comme premier choix.   

![](../images/IMG_20201001_145215.jpg)



## **1.2 Programme de modélisation 3D : Fusion 360** ##
Astuces que j'ai apprises :

* Logo maison à côté du cube pour voir les différentes vues = Retourner à la vue de départ
* Racourci pour l'orbit (se déplacer autour de l'objet) = Shift + molette
* Pour créer (spécifique à mon ordinateur) = rester enfoncé sur le clic gauche puis sélectionner l'action que je veux dans la liste
* Activer log vieuw pour que l'objet ne bouge pas quand je fais le rendu (dans settings)

**Erreur** : Ne pas oublier de créer un nouveau plan (voir barre supérieur en haut vers la droite) lorsque je créer plusieurs objets dans la même vue mais ''pas à la même profondeur '' --> penser à l'exo avec la création d'un tube le long d'une polyligne dont les bouts étaient un cercle et un hexagone.

## **1.3 Platforme de documentation Gitlab** ##

Pour référencer notre documentation, il y a **deux méthodes possibles**. Soit directement **modifier** le texte **sur** la plateforme **GitLab** soit en réaliser les **modification à distance** sur notre PC et puis les synchroniser sur GitLab. Pour ces deux méthodes nous aurons besoin d'utiliser le **langage Mkdocks** mais la méthode à distance permet d'être plus rapide.

### **1.3.1 les bases pour savoir écrire en Markdown** ###
* Ecrire en itallique : `_ this _`
* Ecrire en gras : `** this **`
* Ecrire en gras et itallique : `** _ this _ **`
* Ecrire les sous-titres/titres (Il existe 6 tailles de police différente) :
   * 1er titre (taille la plus grande) : `# This #`
   * 2ème titre (taille plus petite que le 1er titre) : `## This ##`
[…]
* Mettre un lien internet : ex: `[Nom de la référence] (www.github.com)`
* Appliquer une image de référence sur la page : `! [] (../images/nom.png)`
* Citation : `’’texte’’`
* Faire une liste :
  * Liste désordonée : mot précédé de `*`
  * Liste ordonée : mot précédé du numéro : `1.`

**Remarques** :
* Les espaces sont à retirer pour les 6 premiers points abordé sur le langage Markdown.
* Il faut faire des espace entre `*` et le mot pour obtenir l'effet voulu.

### **1.3.2 Rajouter des images sur mon GitLab** ###
1. Aller sur le projet auquel je veux rajouter des images
2. Aller dans mes documents
3. Aller dans mes images
4. Cliquer sur le petit **+** et télécharger un nouveau fichier
5. Une petite fenêtre s'ouvre dans laquelle vous allre venir glisser votre nouvelle image.
6. Votre image à bien été téléchargée. Veillez à entrer le nom exacte de l'image lorsque vous voulez la faire apparaitre dans votre documentation.

![](../images/téléchargerImageexplications.jpg)

**Remarque**: Avant de télécharger toute image sur votre GitLab, il vous faut en réduire au maximum la taille/le poids afin de ne pas surcharger la platforme GitLab sachant qu'un image prise avec son téléphone fait environ 1Mo ce qui est beaucoup trop.

### **1.3.3 Méthode 1 : Modifier son texte sur Gitlab** ###

1. Aller dans mes projets
2. Cliquer sur mes documents. Là je peux voir qu'il y a un dossier qui s'appelle `modules`. C'est là où je dois référencer chaque "notion" que l'on apprend toutes les semaines. (ex: module01 = Etat de l'art et documentation)
3. Cliquer sur Edit pour modifier le contenu du module. Le texte est à écrire en langage Markdown
4. Fois les modifications du texte faite, appuyer sur Commit changes et la mise à jour se fera automatiquement.

**Remarque:** Pour modifier ma présentation personnelle ,sur laquelle on tombe une fois avoir sélectionner le projet sur lequel on travail,  il faut cliquer sur `index.md`



### **1.3.4 Méthode 2 : Modification à distance : premiers pas vers le codage** ###
[Processus à suivre pour toutes informations complémentaires](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#install-git)

Ci-dessous sera exposé la méthode à suivre pour les ordinateurs **Windows 10** comme le mien. Pour tout autre ordinateur, veuillez consultez le lien ci-dessus afin de connaître les petites spécificités qui diffèrents. Je vous conseille également d'aller voir la page de Clémentine [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/clementine.benyakhou/-/blob/master/docs/modules/module01.md) si vous désirez approfondir vos connaissances.

- 1. Installer **Git** ([télécharger Git ici](https://git-scm.com/)) et ouvrir un **shell de commande** en écrivant cmd dans votre barre de recherche. Shell Bash est déjà présent sur les odinateurs Windows 10 donc il n'y a pas besoin de le télécharger. Cependant, cette fonction doit être activée.

En premier lieu, allez dans vos paramètres - Mise à jour et sécurité et passez en **mode développeurs**. Ensuite, recherchez dans votre barre de recherche **fonctionnalités facultatives**. Une fenêtre s'ouvre et vous devez aller dans **plus de focntionnalités Windows**. Là, il vous faut cocher **sous-système Windows pour Linux**. Pour finir, vous serez invitez à **redémarrer votre ordinateur** afin qu'il soit fonctionnel.

![](../images/1._Git_et_Shell_Bash.jpg)

 - 2. Une fois, votre ordinateur rallumé, il faut s'assurer que Git est bien installé. Pour ce faire, tapez dans la commande Git Bash : `git --version`

![](../images/6._Vérifier_que_Git_est_bien_installé.JPG)

 Le processus est réussi ! Vous avez créé Git Bash qui vous permettra de relier la plateforme GitLab à votre ordinateur.

 - 3. Une fois cela effectué, il vous faudra maintenant saisir vos informations d'identification sur Git (nom d'utilisateur et e-mail) pour vous identifier en tant qu'auteur de votre travail. Le nom d'utilisateur et l'adresse e-mail doivent correspondre à ceux que vous utilisez sur GitLab.

![](../images/7-Relier_Gitlab_à_Git_Bash.JPG)

 - 4. Il vous faut maintenant **connecter votre ordinateur à GitLab**. Afin d'assurer au maximum la sécurité de vos données vous pouvez vous identifiez soit via HTTPS soit via SSH. J'opte pour le choix d'une clé SSH qui permet de m'authentifier en ne devant m'identifier que lors de la première utilisation. Ne disposant pas de clef SSH, il me faut en générer une nouvelle. Il existe 2 types de clefs SSH (clef RSA ou clef ED25519). Sous les conseils de Clémentine, je décide de créer une clef ED25519 car elles sont plus sures et performantes.

Pour ce faire taper dans Git Bash : `ssh-keygen -t ed25519 -C "<comment>"` et puis sur enter jusqu'à obtenir la clef qui ressemble à ceci :

![](../images/8-Création_d_une_clef_SSH.JPG)

 Une fois cette clef obtenue, je peux la retrouver sur mon ordinateur. Pour ma part, elle se trouve sur le disque C dans le dossier ssh.  

 ![](../images/9-trouver_la_clef_SSH_sur_mon_PC.JPG)

 - 5. L'étape suivante est d'ouvrir cette clef pub avec Atom (il s'agit d'un éditeur de texte à télécharger [ici](https://atom.io/)). Vous obtenez ceci :

 ![](../images/12-ouvrir_la_clef_publique_avec_atom_pour_la_lire.JPG)

- 6. Copier/Coller et ajoutez la clef sur GitLab. Pour ce faire aller sur votre profil - clés SSH

![](../images/Cléf_SSH_sur_GitLab.jpg)

- 7. Afin de vérifier l'identification, tapez dans GitBash : `ssh -T git@gitlab.com` et ceci apparaitra :

![](../images/14.1.JPG)

**Remarque**: N'oubliez pas de répondre à la question par yes pour que la connexion se fasse

- 8. Maintenant que Git est connecté à GitLab, il vous faut cloner la clef SSH afin de créer un dossier sur votre ordinateur qui comprendra les dossiers présents sur votre GitLab.
En premier lieu, aller copier/coller la clef SSH sur Git Bash mais **n'oubliez pas** de précéder la clef de : `git clone`

![](../images/cloner_doc_sur_son_pc.jpg)

**Remarque** : Vous devez ouvrir un Git Bash à partir du nouveau dossier que vous avez créé sur votre ordinateur (clic droit) sinon cela ne fonctionnera pas !

- 9. Vous avez enfin réussi à cloner vos documents du GitLab sur votre ordinateur !! YOUPIII. Maintenant, vous allez pouvoir modifier vos dossiers GitLab sans connexion et à partir de votre bureau. Avant de commencer cette étape, **vérifiez** que vous ayez bien installé **Atom** ainsi que la **package markdown-preview-enhanced** que vous pouvez télécharger [ici](https://atom.io/packages/markdown-preview-enhanced?fbclid=IwAR0xegr_mDi-_HjRkNP35CowmpaysK5R5hv2oNd9BGsM6X9LUN41474YyLg). Si l'installation ne se lance pas malgré que vous ayez installé Atom, suivez les instructions suivantes :

* Ouvrez Atom, faites Install package et tapez le nom du package : `package markdown-preview-enhanced`

![](../images/18-télécharger_le_package_sur_atom.JPG)

- 10. Maintenant il vous faut ouvrir vos dossiers sur Atom. Cliquer sur Open a Project et aller sélectionner votre dossier où se trouvent tous vos dossiers cloner du GitLab.

![](../images/19-ouvrir_dossier_ds_atom.JPG)

- 11. Votre dossier est ouvert. Normalement, vous le voyez apparaitre à gauche de votre écran. cliquer sur le dossier que vous voulez modifier et il apparaitre au centre de votre écran comme sur l'image ci-dessous. A gauche, en blanc, vous pouvez directement voir le résultat de votre mise en page, Ce qui je trouve est très pratique et vous permet de vous relire et corriger directement sans avoir une sauvegarde "inutile" sur GitLab.

![](../images/20-Modifiersontextesuratom.JPG)

- 12. Une fois le résultats satisfaisant selon-vous, il vous faut maintenant synchroniser votre version d'Atom sur GitLab . Pour ce faire, je vous invite à suivre le point 7 de ce [tuto](https://courses.cs.washington.edu/courses/cse154/19su/resources/assets/atomgit/windows/?fbclid=IwAR1-tPPWdvu7JGnjk1-oEmhNysHqEJHXIt_TQIFxmko2HCB61BI8_Vu0zfY) qui est très bien expliqué. Néanmoins, je vais vous faire un résumé ci-dessous.
  - 1. Appuyer sur Stage All
  - 2. Vous pouvez voir appaitre votre dossier dans la case Staged changes
  - 3. Vous pouvez nommer votre changement afin de les retrouver plus facilement si vous avez besoin de revenir en arrière.
  - 4. Une fois cela fait, appuyer sur Commit to master
  - 5. Pour finir,appuyer sur **Pull** pour vous assurer que toutes modifications faites à partir de GitLab a été mise à jour sur votre version sur Atom. Une fois cela fait, il ne vous reste plus qu'à appuyer sur **Push** et la synchronisation sur GitLab se fait automatiquement. Il ne vous reste plus qu'à vérifier sur votre GitLab !`

  ![](../images/21-synchroniseravecgitlab.JPG)

  **Remarque**: Tant que vous n'avez pas modifié votre texte sur Atom, **Push** et **Pull** ne sont pas visibles. Il est écrit à la place **Fetch** qui vous permet de voir si il y a eu des modifications sur GitLab non mises à jour sur Atom. Pour faire apparaitre **Push** et **Pull** s'ils n'apparaissent pas même après avoir fait des modifications de texte sur Atom, faites un clic droit sur **Fetch**.

**P.S.**: N'oubliez pas d'à chaque fois sauvegarder votre fichier qui est ouvert sur Atom dès que vous faites des modification.

  #### Voilà ! C'est enfin terminé, vous savez maintenant modifier les dossiers de votre GitLab à distance !!! ####
