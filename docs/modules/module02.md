# 2. Conception assistée par ordinateur

![](https://www.aplicit.com/wp-content/uploads/2019/10/Logo-Fusion-360-e1571412892994.png)

Pour cette séance, l'enjeux sera de **modéliser en 3D** son objet choisi au musée Adam (la chaise universale de Joe Colombo pour ma part). Pour ce faire, nous allons utiliser le logiciel **Fusion 360** proposé par le suite Autodesk. C'est un logiciel payant dont nous bénéficions l'accès grâce à une license étudiante. Vous pouvez néamoins télécharger une version d'essaie [ici](https://www.autodesk.com/products/fusion-360/overview).

## 2.1 Rappel des bases sur Fusion 360 ##
Si, tout comme moi, vous faites vos premiers pas sur le logiciel Fusion 360, je vous invite à suivre des tutoriels. Ceux que j'ai utilisé sont référencer dans le bas de la page. En attendant, voici un petit récapitulatif des commandes de bases :

* **La maison** à coté du cube avec les différentes vues : Retourner à la vue de "base" sur votre objet.

![](../images/maison.JPG)

* **Arbre de construction** permet de voir toutes les étapes effectuées lors de la conception. Il est toujours possible de revenir modifier l'une d'elle en déplaçant le curceur jusqu'au moment voulu.

![](../images/Arbre_de_construction.JPG)

* **Barre d'outils** depuis laquelle vous pouvez créer et effectuer toutes sortes de modifications

![](../images/outilsFusion360.JPG)

* Ici vous pouvez voir toutes **vos esquisses et corps** que vous avez céés. Vous pouvez les masquer ou les faire apparaitre à l'aide du petit oeil. Il vous est possible de créer des objets à partir d'**esquisses** en **2D** ou de créer à partir de l'**outil form** (cube mauve) en **3D**. Ces deux outils sont distincts sur le logiciel. Il est impossible d'intéragir en passant de l'un à l'autre. Vous devez soit créer avec le mode 2D soit avec le mode 3D. Ce n'est qu'un fois chaque pièce terminée que vous pourrez les assembler peu importe si par exemple elles n'ont pas été conçue toutes les deux avec le procéder esquisse.

![](../images/Voircequej_aicréé.JPG)

* Activer **log vieuw** dans les paramètres pour l'objet ne bouge pas lorsque vous faites un **rendu**
* **Lignes noirs** = lignes fixées
* **lignes bleues** = Lignes que l'on peut encore déplacée
* **Sauvegarder son fichier** Fusion 360 sur son ordinateur : `aller sur la feuille - exporter`

![](../images/sauvegardersonfichier.JPG)

* Raccourci pour **l'orbit** : `Shift + molette`

![](../images/orbit.JPG)

* Raccourci pour **rectangle** : `R`
* Raccourci pour **ligne** : `L`
* Raccourci pour **cercle** :  `C`
* Raccourci pour **les dimensions** : `sélectionner la ligne + D`
* Raccourci pour **déplacer une objet** : `M`

**Remarque** :
* Tous les raccourcis sont noté à chaque fois à coté du nom de la modification que vous allez effectuer. **Exemple** : Pour extruder je vais dans : créer - extrusion et je peux voir à coté qu'il est écrit `E`.
* Lorsque je veux dérouler par exemple l'onglet créer et que celui-ci ne veut pas avec un simple clic, il faut rester enfoncé sur son clic droit.

## **2.2 Recherches des mesures pour modéliser mon objet**
* Recherche des mesures sur internet
  * [Visit bellelurette](https://www.bellelurette.eu/produit/universal-de-jo-colombo/)
  * [Visit edit-revue](http://edit-revue.com/?Article=166)
  * [Visit design-market](https://www.design-market.fr/56_colombo)
  * [Visit design-market](https://www.design-market.fr/134526-chaise-vintage-mod%C3%A8le-4867-universale-de-joe-colombo-pour-kartell-1970.html)
* Dessiner l'objet pour mieux le comprendre
* Prendre une photo de l'objet et la mettre à l'échelle sur Autocad pour connaître des mesures qu'il me manque

### 2.2.1 Mise à l'échelle d'une photo +grande sur Autocad
Manquant d'informations pour directement dessiner mon objet sur Fusion 360. Je décide, d'importer une **photo frontale** de ma chaise sur Autocad. Il faut maintenant mettre cette image en taille réelle afin de connaitre les dimensions qu'il me manque. Cette manipulation peut être un peu compliquée, c'est pourquoi je vous explique ci-dessous comment faire pour **mettre une photo à une taille précise**.
1. Tracer deux lignes, l'une correspondant une dimension sur votre chaise en photo (**ex : la largeur de l'assise) et l'autre la longueur réel que cette dimension doit avoir. Veillez à bien superposer ces deux lignes afin qu'elles aient la même origine.
2. Echelle + sélectionner l'objet + entre
3. Sélectionner le coin où les deux longueurs se rejoignent (la bonne et la +grande)
4. r + entre
5. Sélectionner la +grande longueur en commencant par le coin où les 2 longueurs se rejoignent
6. Sélectionner le point où j'arrive à l'extrémité de la longueur voulue
7. Enlever la distance en trop en cliquant sur l'extrémité de la bonne ligne  

![](../images/sample-pic-2.jpeg)

Voilà ! Maintenant vous êtes capable de mesurer n'importe quelle dimension sur votre dessin. Vous pouvez également directement **importer ce fichier au format DXF sur fusion 360**. Le dessin sera directement reconnu comme esquisse que vous pourrez modifier dans Fusion 360 même. Pour ce faire aller dans : `insérer - insérer un fichier DXF`

## **2.3 Tests afin de réussir à modéliser l'objet chaise universale sur Fusion 360**

![](../images/Photomodèleréel.jpg)

Après avoir passé du temps à imaginer comment aborder l'objet dans sa conception, celui-ci sera réalisé en **3 morceaux (Pieds, assise + dossier et strucuture à l'intérieur des pieds pour assembler le tout)**. Par la suite, je me rends compte que je dois également **séparer l'assise du dossier** du à la complexité de celui-ci.

### 2.3.1 Test n°1 - Assise ###
* **Modélisation à partir de l'outil esquisse**
Dans un premier temps, je conçois **l'assise et le dossier comme un même corps**. Je commence par  la réalisation de l'assise avec `esquisse + extrusion`. Le rendu n'est pas assez proche de la réalité. L'assise est toute droite et plate ce qui n'est pas le cas en réalité. Afin de remédier à cela, je vais créer une esquisse à l'aide de l'**outil spline + extrusion**

![](../images/assise_place_-_courbée.jpg)


### 2.3.2 Test n°2 - Tests pour réaliser le dossier ###
* **Modélisation à partir de l'outil esquisse**
Une fois que je suis sastifaite de la forme de l'assise. Je m'attaque au dossier. Pour ces 2 rendus, j'utilise à nouveau l'`outil spline + extrusion` mais dans des plans différents. On peut voir que le rendu n°2 est légèrement plus satisfaisant. Mais le **rendu ne se rapprochant toujours pas de la forme réel** de l'objet qui est plus organique, j'expérimente l'`outil forme` (cube mauve) qui va me permettre de moduler à ma guise une surface/forme.

![](../images/Dossier.jpg)

### 2.3.3 Test n°3 - Dossier à l'aide de l'outil forme ###
* **Modélisation à partir de l'outil forme**
Pour ce test j'ai donc **séparer la modélisation de l'assise en 2** : l'assise + le dossier. On peut constater que le dossier est nettement plus dans l'esprit de rendu de la chaise réel mais je n'ai toujours pas réussi à modélier la même forme. De plus, l'assemblage des 2 formes : assise + dossier rend un aspect étrange. On voit clairement que les 2 pièces n'ont pas été réalisées à l'aide des mêmes outils (l'un forme et l'autre esquisse.) Pour utiliser l'outil forme, j'ai `créer un rectangle`surlequel je suis ensuite venue `ajouter des arrêtes et des points` pour pouvoir mieux **sculpter le dossier**. A chaque fois que vous voulez modifier votre forme, il vous suffit de `sélectionner l'objet - clic droit - modifier forme`

![](../images/Dossier_sculpted.JPG)

### 2.3.4 Test n°4 - Découvrir l'outil modifier une face ###
* **Modélisation à partir de l'outil esquisse**
Grâce au module Fusion 360 documenté par Baptiste, que vous pouvez trouver [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/baptiste.sarazin/-/blob/master/docs/modules/Module%2002%20-%20FUSION%20360%20(Conception%20assist%C3%A9e%20par%20ordinateur).md), j'ai découvert un nouvel outil qui permet de **"sculpter" un objet produit à partir d'esquisses**. Vous pouvez également consulter **[ce tuto](https://www.youtube.com/watch?v=OMaZyQCM6_o)** et également **[celui-ci](https://www.youtube.com/watch?v=LcrD-MwguXY)** qui sont très bien expliqués.

Ci-dessous, vous trouverez les étapes que j'ai effectué :
1. **Insérer le fihier DXF** produit à partir d'Autocad. Mon unité sur Autocad est en mm. Afin qu'il soit en taille réel sur Fusion 360, j'ai dû changer l'unité de mon objet en cm lorsque je l'importe (voir point B.).

![](../images/ImporterDXF.jpg)

2. **Extruder l'esquisse**. Ensuite, Créer une **nouvelle esquisse** sur les "languettes". Enfin, **extruder cette nouvelle esquisse** jusqu'à la "languette" opposée.

![](../images/Extrusiondossieretassise.jpg)

3. **Evider les pieds de la chaise** en créant des nouvelles esquisses puis extruder. L'outil coque est censé donné le résultat mais ici il ne voulait s'appliquer.

![](../images/10.JPG)

4. Utiliser l'outil congé pour que les **angles de l'assise soient arrondis** + **Réaliser le trou dans la chaise** avec : esquisse + extruder.

![](../images/congé+extruduertrouchaise.jpg)

5. Une fois la **base de votre chaise réalisée**, vous aller maintenant créer un **nouvelle élément** à l'aide de l'**outil forme** (cube mauve). Choississez **créer un plan** que vous allez pouvoir sculpter dans le but de donner les courbures à la chaise. Une fois cette nouvelle forme prête, vous allez pouvoir **l'appliquer avec votre chaise - modèle de base**.

![](../images/Débuteravecoutilmodifieruneface.jpg)

## **2.4 Tutos suivis pour la réalisation de cet exercice**
* Apprendre à utiliser l'outil sculpted :
  * [Edit sculpted bodies](https://www.youtube.com/watch?v=HEFhu-_ruLA)
  * [Débuter avec le mode sculpture/épisode 1](https://www.youtube.com/watch?v=-sealQihzuY)
  * [Débuter avec le mode sculpture/épisode 2](https://www.youtube.com/watch?v=EcNAwI-Sy-g&list=PLWPVE17OQSYNXVgW7BjCrT94GGkoNTfQt&index=15)
* Apprendre à utiliser l'outil modifier une face qui permet d'avoir un rendu "sculpté"" :
  * [Utiliser l'outil modifier une face-vidéo 1](https://www.youtube.com/watch?v=LcrD-MwguXY)
  * [Utiliser l'outil modifier une face-vidéo 2](https://www.youtube.com/watch?v=OMaZyQCM6_o)
* Mettre un objet à l'échelle lorqu'on importe un DXF ou SVG
  * [Mettre à l'échelle](https://www.youtube.com/watch?v=0_l5S-MtRJw)
