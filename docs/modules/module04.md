# 4. Découpe assistée par ordinateur

## 4.1 Introduction ##
Cette semaine du 19 octobre 2020, j'ai suivi une formation sur la **découpe/gravure au laser**. C'est-à-dire qu'une fois mon dessin réalisé, je peux l'envoyer sur une machine qui à l'aide de son laser va soit **graver** soit **couper** un matériau choisi (**ATTENTION** consultez la liste des matériaux conseillés). Tout **dépend** de la **puissance** et de la **vitesse** à laquelle le laser est réglé. Il faudra veiller à enregistré le fichier dessin sous un **format vectoriel** afin que celui-ci puisse être lu par la découpeuse laser. Le dessin vectoriel permet également d'en modifier la taille sans que celui-ci perde en qualité. 

## 4.2 Listes des différents matériaux ##
**Matériaux recomandés** 
   - Bois contreplaqué (plywood/multiplex)
   - Acrylique (PMMA/Plexiglass)
   - Papier, carton
   - Textiles

**Matériaux déconseillés**
   - MDF : fumée épaisse, et très nocives à long terme
   - ABS, PS : fond facilement, fumée nocives
   - PE, PET, PP : fond facilement
   - Composites à base de fibres : poussières très nocives
   - Métaux : impossible à découper

**Matériaux interdit !**
   - PVC : Fumée acide, et très nocives
   - Cuivre : réfléchit totalement le laser
   - Téflon (PTFE) : fumée acide, et très nocives
   - Vinyl, simili-cuir : peut contenir de la chlorine
   - Résine phénolique, époxy : fumée très nocive  

## 4.3 Carctéristiques des deux machines au FABLAB ##
Le **Fablab** où je me trouve est équipé de **2 machines** : une **Laseraur** (machine open-source) et une **Muse FullSpectrum**. Pour l'exercice qui va suivre, j'ai utilisé la Lasersaur mais avant cela,vous trouverez ci-dessous un tableau regroupant les spécificités des 2 machines. Ainsi, vous serez capable de les utiliser toutes les deux. 

![](../images/pdf_les_émachines.jpg)

## 4.4 Connection aux machines ##
Pour relier un ordinateur à l'une des deux machines (seulement 1 à la fois), il y a deux possibilités. Soit à l'aide du cable RG45 soit en utilisant le réseau wifi _Laser Cutter_ (mot de passe : fablabULB2019). 

[Adresse pour **Lasersaur**](http://192.168.1.3:4444) 
[Adresse pour **Muse FullSpectrum**](http://192.168.1.4)

## 4.5 La pratique pour s'exercer ! ##
Afin de se familiariser avec ce nouvel outil et ces nouvelles notions, il a été demandé de réaliser un luminaire uniquement à partir d'une feuille de polypropylène (45x70cm).



- 1. D'abords, j'ai **réalisé mon dessin** sur **Autocad** en veillant à créer différents calques. Les couleurs des calques seront reconnues sur l'application Drive BoardApp qui permet de lancer la découpe et nous pourrons ainsi attribuer une vitesse et puissance à chaque couleur. 
J'auraiS également pu dessiner sur **Illustrator ou encore Inkscape**. **La seule chose à retenir** est que mon **fichier dessin** doit être enregistré **sous** le **format SVG ou DXF pour lancer la découpe/gravure avec l'application Drive BoardApp**.

**Remarque**: Lorque vous concevez votre projet sur **Autocad**, il est impossible d'enregistré directement votre fichier sous le format SVG. Il vous faudra l'enregistré en **DXF**. 
- 2. Une fois votre fichier DXF enregistré sur votre ordinateur,**ouvrez le sur Inkscape** (Programme téléchargeable gratuitement sur votre ordinateur ou déjà installé sur l'ordinateur du Fablab) --> [télécharger Inkscape ici](https://inkscape.org/fr/release/inkscape-0.92.4/). Si vous possédez la dernière version d'Inkscape, une petit fenêtre s'affiche automatiquement lorsque vous ouvrez votre fichier. Celle-ci vous demande si vous voulez réduire l'échelle de votre dessin. Cette option est très pratique lorque vous voulez faire par exemple un test ou que vous avez mal dimensionné votre dessin. Si vous voulez **découper votre objet en taille réel, aucun paramètre ne doit être changé**. Vous devez **uniquement enregistré** votre fichier **sous le format SVG** car pour le moment importer des fichier DXF sur l'application Drive BoardApp de l'ordinateur du Fablab ne fonctionne pas. 

**RAPPEL** : Si vous avez travaillé sur un programme vous permettant d'enregistré directement votre fichier en SVG (ex: Illustrator), il n'est pas nécessaire de repasser par Inkscape **sauf** pour placer le début de votre dessin dans le coin supérieur gauche de la feuille A4 d'Inkscape. Cette opération, a pour but de directement placer votre dessin proche de l'origine du Laser lorsque vous l'ouvrez sur l'application Drive BoardApp. Ainsi, vous éviterai la perte de matériau inutile. 

- 3. A cette étape, il vous faut ouvrir votre fichier sur l'application Drive BoardApp et attribuer une **vitesse (F)** et une **puissance (%)** pour chaque couleur en fonction de vos envies (gravures ou découpes). Je vous conseille de paramétrer en premier lieu les réglages pour la gravure et ensuite pour la découpe afin d'éviter que le matériau n'aie bouger suite aux découpes. Les réglages à définir dépendent des matériaux et peuvent être trouvé sur internet. Le Fablab est déjà muni de différents tests surlesquelles nous pouvons nous baser. 

![](../images/calque_réglages.JPG)


- 4. Une fois tous les réglages paramétrés, il vous faut avoir **allumer la Lasersaur** et ce qui lui permet de fonctionner en toute sécurité.
![](../images/Etape_pour_allumer_Lasersor.jpg)


- 5. Placer votre planche de matériaux dans la machine. **Lancer le périmètre** afin de vérifier que votre dessin ne sorte pas de votre matériaux. Pour ce faire, cliquer sur la petite maison pour que le laser revienne à son origine et cliquer ensuite sur les deux petites flèches. Si votre dessin est trop grand, resppasez par Inkscape pour réduire l'échelle.
![](../images/Périmètre.jpg)


- 6. Si tout est OK, vous pouvez cliquer sur **Run pour lancer la découpe/gravure**. Maintenant, il ne vous reste plus qu'à attendre pour voir le résultat (le temps s'affiche sur l'application Drive BoardApp) !


- 7. Une fois la découpe terminée, attendez jusqu'à ce que la fumée soit extraite. Celles-ci sont naucives pour la santé !


**Remarque** : Pour plus de précision sur l'allumage de la Lasersaur, vous pouvez [consultez la manuel](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/files/Manuel_Lasersaur.pdf) que j'ai reçu lors de ma formation.



## 4.6 Evolution en plusieurs étapes de mon luminaire  ##

### 4.6.1 Pourquoi cette forme ? ###
Pour réaliser mon luminaire, je me suis inspirée des différents concepts que possèdent mon objet choisi au musée ADAM. En effet, tous comme les pieds de ma chaise, il s'agit d'un **emboitement** de deux ensembles dont les pièces qui les constituent sont facièlement **démontables** à l'aide d'un système de clips. Ma dernière envie fût de réaliser un lumière dont on puisse également **modifier** l'ambiance lumineuse. Pour ce faire, j'ai réalisé un cylindre à bases octogonales dont le motif d'ouverture se répèteraient une face sur deux. Ainsi, il est possible de placer l'ensemble extérieur en deux positions.

 ### 4.6.2 Illustration de l'évolution ###
- 1. Tentative de modélisation de mon concept avec une feuille de papier puis avec la feuille de polypropylène. **Echec** au niveau des attaches, à réfléchir ! 

![](../images/lampe_tests.jpg)

- 2. Sytème  opérationnel. Toutes les pièces n'ont plus qu'à être assemblées.

![](../images/lampe_montage.jpg)

- 3. Résultat une fois les pièces assemblées

![](../images/Lampe_resuslat_final.jpg)

- 4. Résultats final du luminaire dans le noir. L'abat jour est placé dans les 2 positions possibles afin de comparer l'ambiance lumineuse. Celle-ci ne varie quasiment pas. C'est un échec au niveau de la modélisation du concept d'adaptabilité que j'ai tenté de matérialiser.  

![](../images/lampe_résultat_final_dans_le_noir.jpg)

