# 3. Impression 3D #

Lors de cette séance, nous allons apprendre à utiliser les imprimantes 3D. Pour ce faire, il faut au préalable avoir exporté un **fichier** de Fusion 360 (= programme de modélisation 3D) au format **STL** et avoir télécharger le **logiciel PrusaSlicer** sur son ordinateur. Chaque imprimante possède son propre logiciel d'impression. Le fablab où je me trouve est quant à lui équippé de l'imprimante PRUSA I3MK3S ([télécharger le logiciel PrusaSlicer spécifique à cette machine ici](https://www.prusa3d.com/drivers/)).

**N.B.** : Exporter son fichier Fusion 360 sous format STL afin de l'enregister sur son PC puis l'importer en l'ouvrant sur PrusaSlicer.

## **3.1 Modélisation de l'objet : chaise universale** ##
Avant d'imprimer quelque chose, il faut bien évidemment l'avoir dessiné au préalable. N'ayant pas encore complètement réussi à modéliser mon objet choisi au musée ADAM sur Fusion 360 au module précédent, j'ai décidé de lancer l'impression uniquement des pieds de ma chaise. En effet, cela ne pose pas de soucis étant donnée que les pieds sont démontables.

### **3.1.1 Modélisation de l'enveloppe des pieds** ###
1. Dessinez une esquisse, dans un plan choisi, qui va nous servir de base pour extruder et avoir les bonnes dimensions du pied. Puis faites terminer l'esquisse.
**N.B.**: Noubliez pas que vous pour transformer des lignes en lignes de construction et utiliser les petits ciseaux afin d'obtenir la forme que vous désirée.
2. Sélectionnez votre esquisse et avec l'outil extruder rentrer la hauteur que vous voulez que l'objet fasse.
3. Utilisez l'outil coque afin de creuser l'objet tout en laissant une épaisseur à l'objet.

![](../images/Modélisationpieddelachaise.jpg)

### **3.1.2 Modélisation de la structure intérieure des pieds** ###
Il est très simple de modéliser cet objet. Il vous suffit de créer des esquisses puis d'extruder à chaque fois. Attention, vérifiez bien les mesures par rapport a l'enveloppe du pied créé précédemment. Cette étape est importante étant donnée que la structure doit s'insérer à l'intérieur de l'enveloppe. C'est grâce à ce système que les pieds sont ensuite fixés à l'assise et au dossier donc il faut être extrêmement précis !

![](../images/Structurepied.JPG)

## **3.2 Imprimante 3D (PRUSA I3MK3S = imprimante utilisée au Fablab de l'ULB)** ##

**Remarques**:

* l'imprimante PRUSA est une imprimante qui à été conçue à pâtir d'autres imprimantes 3D ce qui est très pratique lorsque l'on veut remplacer une pièce.
* Toujours importer des **Fichier STL** pour imprimer sur une machine 3D !
* Veillez à bien positionner l'objet sur le plateau une fois celui-ci mis l'échelle **(voir point 3.1.1)**. Pour ce faire, aidez-vous de la barre d'outils sur le coté gauche de votre écran (déplacer, rotation ect) !
* Passez en mode expert (en haut à droite de la page --> **voir photo 2.**) afin d'être plus précis dans les réglages d'impression **(voir photo 1.)** **ET** veillez à bien entrer les paramètres de la buse et le nom de l'imprimante (**voir photo 2**). Sur la 3ème photo (**voir photo 3.**), on peut constater sur l'écran de l'imprimante 3D que j'avais oublié cette étape. Il est donc indiqué 0° pour la température à laquelle fond le filament.
* **Attention**, encodez correctement tous les paramètres d'impression **(voir point 3.1.2)** qui se trouvent en haut à gauche de la page sur le logiciel PrusaSlicer (**voir photo 1.**)

![](../images/0.1.JPG)

### 3.2.1 Mettre à l'échelle votre objet pour qu'il rentre sur le plateau  ###
* Changez cette valeur à votre guise (entouré en rouge) du moment que l'objet rentre sur votre plateau (si ce n'est pas le cas, vous aurez un message d'alerte où le fond derrière le plateau sera rouge). N'oubliez pas qu'au plus votre objet est grand au plus celui-ci mettra du temps à être produit !

![](../images/12.1.jpg)

**Remarque**: Le périmètre permet de vérifier que l'objet soit dans la zone prévue --> voir la place qu'occupe l'objet sur le plateau (ligne qui l'entoure)

### 3.2.2 Paramètres d'impression ###

#### a. Couches et Périmètres ####
* Positionner les deux paramètres couches **en général sur 0,2** (entouré en bleu)

  ##### a.1 Parois verticales #####
   * L'**idéale** est d'avoir **3 couches** pour les parois verticales (entourés en rose). Au moins il y a de couches, au plus la matière de l'objet sera transparente et ''souple''.

![](../images/2.JPG)



#### b. Remplissage ####
* Régler la densité --> **en général sur 10-15%**
Il ne faut **jamais dépassé 35%** car cela ne change rien au niveau de la résistance mécanique de l'objet conçu.
* Les différents **''modèles'' utilisés** généralement pour remplir un objet : **Triangle - nid d'abeille - giroïde**

![](../images/14.JPG)

#### c. Jupe et bordure ####
* A mettre si l'objet est élancé
* Permet que l'objet ne se décolle pas du plateau lorsque celui-ci est long et élancé.
* **3-4mm en général** --> paramètrage par défaut


#### d. Supports ####
* Quand l'angle d'une surface est supérieur à 45° où qu'il y a des porte-à-faux, il faut rajouter des supports afin de stabiliser l'objet. La plupart du temps un support uniquement sur le plateau suffit.

![](../images/b.jpg)

**Remarque**: L'augmentation de l'espacement du motif du support permet de le retirer plus facilement après impression

### 3.1.2 REGLAGES DU FILAMENT ##
* T° à laquelle le filament fond sur cette machine. On peut trouver ces informations sur la bobine du filament.

![](../images/10.1.jpg)

### 3.2.3 Enregistrer le fichier sur votre PC une fois tous les réglages faits ###
* C'est bon vous avez terminé en ce qui concerne les réglages sur PrusaSlicer ! Revenez sur plateau et **enregistrez le fichier** sur votre PC en cliquant sur **Découper maintenant** en bas au droite de votre écran. Le fichier sera enregistrer sous le format gcode.

![](../images/11.JPG)

### 3.2.4 Etapes pour lancer l'impression  ###

1. Munissez-vous d'une carte SD surlaquelle vous viendrez y copier votre ficher gcode.
2. Nettoyer le plateau de la machine 3D avec un papier et de l'acétone pour améliorer l'adhérence.
3. Mettre la carte SD dans la machine où j'y ai créé un dossier à mon nom.
4. Cliquer sur mon fichier.
**IMPORTANT**  **La vitesse ne doit pas dépassée 100% !** et pour une **petite pièce compliquée**, qui prend du temps passé à du **50-60%**
5. Rester à côté de la machine pour les 3 premières couches afin de voir que tout se passe bien.
6. Voilà ! Rendez-vous dans quelque minutes/heures/jours en fonction de votre pièce (Le temps est indiqué sur le petite écran de la machine).

### 3.2.5 Résultat ###
Avec l'aide de Gwen et Helen, j'ai réussi à imprimer sans trop soucis excepté le petit oubli mentionné au point 3.2 photo n°3. Voici des photos du résulat. Il y a malheureusement un soucis de dimensionnement. La structure du pied est bien trop grande pour rentrer dans l'enveloppe...

![](../images/Résultatimpression3D.jpg) 
